import { Component, OnInit, Inject } from '@angular/core';
import { CorekService } from '../services/corek/corek.service';
import { MAT_DIALOG_DATA, MatDatepickerInputEvent } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import {MatPaginator, MatTableDataSource, MatSort, MatDialogModule, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS} from '@angular/material';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MMM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class DetailsComponent implements OnInit {
  fechaI;
  fechaF;
  today;
  albumsId = [1, 2, 3];
  displayedColumns: string[] = ['actividades'];
  dataSource;
  historial = [];
  constructor(public corek : CorekService, @Inject(MAT_DIALOG_DATA) public data: any) { 
    console.log(this.data);
  }

  ngOnInit() {
    this.today = new Date();
  }

  procesar(){
    let inicial = this.fechaI.format('YYYY/MM/DD');
    let final = this.fechaF.format('YYYY/MM/DD');
    console.log(inicial);
    console.log(final);
    const time = Date.now().toString();
    console.log("SELECT * FROM `wp_posts` WHERE (`post_author` =" +this.data+ " ) AND (`post_date` >= '"+inicial+" 00:00:00' AND `post_date` <= '"+final+" 23:59:59') AND `post_type` LIKE 'actividades'");
    this.corek.socket.emit('query',{'querystring': "SELECT * FROM `wp_posts` WHERE (`post_author` =" +this.data+ " ) AND (`post_date` >= '"+inicial+" 00:00:00' AND `post_date` <= '"+final+" 23:59:59') AND `post_type` LIKE 'actividades'", 'event':time + 'rango'});
    this.corek.socket.on(time + 'rango', (data , key) => {
      console.log(data);
      for (let r of data) {
        let hour = new Date(r.post_date);
        let y = new Date(hour.setDate(hour.getDate() + 0));
        let x = new Date(hour.setHours(hour.getHours()+4));
        this.historial.push({actividad:r.post_content,fecha:y, hour:x});
      }
      console.log(this.historial);
    });
  }

  search(){
    console.log(this.fechaI, this.fechaF)
    if(this.fechaI != undefined && this.fechaF != undefined){
      this.procesar();
    }
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    // this.events.push(`${type}: ${event.value}`);
    console.log(event.value);
  }

}
