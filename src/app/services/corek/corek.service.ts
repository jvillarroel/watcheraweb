import { Injectable } from '@angular/core';
import * as io from "socket.io-client/dist/socket.io";
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CorekService {
  socketHost: string = "https://v2.corek.io:8095";
  socket: any;
  server: any;
  constructor(private router:Router) { }

  public ConnectCorek(){
    this.socket = io.connect(this.socketHost,{'reconnection':false});
  }

  public ConnectCorekconfig(nf){
    this.socket = io.connect(this.socketHost,{'reconnection':true});
   
    this.socket.on('connection', (data)=>{
      this.socket.emit('conf', { 'project': 'http://watcher.com','event':nf});
    });
  }
}