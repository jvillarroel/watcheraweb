import { Component } from '@angular/core';
import { CorekService } from './services/corek/corek.service';
import { RouterModule } from '@angular/router';
import { MatTableDataSource, MatDialog } from '@angular/material';
import {Router} from '@angular/router';
import { DetailsComponent } from './details/details.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  albumsId = [1, 2, 3];
  displayedColumns: string[] = ['name', 'actividad', 'opcion'];
  dataSource;
  public show:Boolean = false;
  constructor(public dialog:MatDialog, public router: Router, public corek : CorekService) {
    let cf = Date.now().toString()+"halamadrid";
    this.corek.ConnectCorekconfig(cf);
    this.corek.socket.on(cf , (d)=>{
      console.log(d);
      const time = Date.now().toString() +'get_user';
      this.corek.socket.emit('get_users',{condition:{},'event':time + 'get_user'});
      this.corek.socket.on(time + 'get_user', (data,key)=>{
        console.log(data)
        let mylista = [];
        let i = 1;
        for (let r of data) {
          console.log(r);
          const timeuno = Date.now().toString() +'get_user'+Math.random();
          this.corek.socket.emit('query_post',{"condition":{'post_author':r.ID,'post_type':'actividades'},'order':'ID','orderby':'DESC','event':timeuno});
          this.corek.socket.on(timeuno,(data1)=>{
            console.log(data1);
            if (data1.length > 0){
              mylista.push({position:r.ID, name: r.user_login,actividad:data1[0].post_content});
              console.log(i, data.length);
              if (i == data.length){
                console.log(mylista)
                this.show = true;
                this.dataSource = new MatTableDataSource(mylista);
              }
              i++;
            }
          });
        }
      });  
    });
  }

  edit(id){
    const dialogRef = this.dialog.open(DetailsComponent, {
      data: id,
      height: '600px',
      width: '600px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      
    });
  }

}
