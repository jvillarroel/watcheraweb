import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CorekService } from './services/corek/corek.service';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { MatDialogModule , MatTableModule  ,MatNativeDateModule ,MatDatepickerModule , MatInputModule ,MatPaginatorModule , MatSortModule , MatTooltipModule , MatListModule , MatBottomSheetModule, MatIconModule} from "@angular/material";
import {MatFormFieldModule} from '@angular/material/form-field';

import { RouterModule, Routes } from '@angular/router';
import { DetailsComponent } from './details/details.component';
const routes: Routes = [
  {
    path:"details",
    component: DetailsComponent 
  },
]
@NgModule({
  declarations: [
    AppComponent,
    DetailsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    RouterModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    FormsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [CorekService], 
  bootstrap: [AppComponent],
  entryComponents:[
    DetailsComponent
  ]
})
export class AppModule { }